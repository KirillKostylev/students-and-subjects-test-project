package by.pstlabs.studentsandsubjectstestproject.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

@Data
@Entity
public class Subject {

    public static final String ID_STR = "id";
    public static final String NAME_STR = "name";
    public static final String HOURS_NUMBER_STR = "hoursNumber";
    public static final String REGEX_FOR_NAME = "[a-zA-ZА-Яа-я]*";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "can't be null or empty")
    @Pattern(regexp = REGEX_FOR_NAME, message = "must consist of characters A-Z, a-z, А-Я, а-я")
    @Size(min = 2, max = 30, message = "must be 2 - 30 characters length")
    private String name;

    @NotNull(message = "can't be null")
    @Min(value = 10, message = "must be greater than 10")
    @Max(value = 10000, message = "must be less than 10000")
    private int hoursNumber;

    @ManyToMany(mappedBy = "subjects")
    @JsonIgnore
    private List<User> users;
}
