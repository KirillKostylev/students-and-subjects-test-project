package by.pstlabs.studentsandsubjectstestproject.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Entity
public class User {

    public static final String REGEX_FOR_EMAIL = "^[\\w-+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
    public static final String REGEX_FOR_PASSWORD = "[A-Za-z0-9_]*";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "can't be null or empty")
    @Pattern(regexp = REGEX_FOR_EMAIL, message = "must be a well-formed email address")
    private String email;

    @NotBlank(message = "can't be null or empty")
    @Pattern(regexp = REGEX_FOR_PASSWORD, message = "must consist of characters A-Z, a-z, 0-9 and '_'")
    @Size(min = 4, max = 30)
    private String password;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "contact_info_id")
    @Valid
    @NotNull
    private ContactInfo contactInfo;

    @ManyToMany
    @JoinTable(
            name = "user_subject",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "subject_id"))
    private Set<Subject> subjects;

}
