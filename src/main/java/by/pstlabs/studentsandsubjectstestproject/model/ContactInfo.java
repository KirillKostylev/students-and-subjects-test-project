package by.pstlabs.studentsandsubjectstestproject.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "contact_info")
public class ContactInfo {
    public static final String REGEX_FOR_FIRST_NAME = "[a-zA-ZА-Яа-я]*";
    public static final String REGEX_FOR_LAST_NAME = "[a-zA-ZА-Яа-я]*";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "first_name")
    @NotBlank(message = "can't be null or empty")
    @Pattern(regexp = REGEX_FOR_FIRST_NAME, message = "must consist of characters A-Z, a-z, А-Я, а-я")
    @Size(min = 2, max = 30, message = "must be 2 - 30 characters length")
    private String firstName;

    @Column(name = "last_name")
    @NotBlank(message = "can't be null or empty")
    @Pattern(regexp = REGEX_FOR_LAST_NAME, message = "must consist of characters A-Z, a-z, А-Я, а-я")
    @Size(min = 2, max = 30, message = "must be 2 - 30 characters length")
    private String lastName;

    @OneToOne(mappedBy = "contactInfo", cascade = CascadeType.ALL)
    @JsonIgnore
    private User user;
}

