package by.pstlabs.studentsandsubjectstestproject.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RoleEnum {
    ADMIN(new Role(1, "Admin")),
    STUDENT(new Role(2, "Student"));

    private Role role;
}
