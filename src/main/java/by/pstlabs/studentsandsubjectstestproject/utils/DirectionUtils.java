package by.pstlabs.studentsandsubjectstestproject.utils;

import org.springframework.data.domain.Sort;

public class DirectionUtils {
    public static Sort.Direction getDirection(String direction) {
        if ("Desc".equalsIgnoreCase(direction)) {
            return Sort.Direction.DESC;
        } else {
            return Sort.Direction.ASC;
        }
    }
}
