package by.pstlabs.studentsandsubjectstestproject.controller;

import by.pstlabs.studentsandsubjectstestproject.model.Subject;
import by.pstlabs.studentsandsubjectstestproject.model.User;
import by.pstlabs.studentsandsubjectstestproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("users")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<Iterable<User>> findAll() {
        return ResponseEntity.ok(userService.findAll());
    }

    @GetMapping("{id}")
    public ResponseEntity<User> findById(@PathVariable Integer id) {
        return ResponseEntity.ok(userService.findById(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<User> create(@Valid @RequestBody User user) {
        User body = userService.create(user);
        return ResponseEntity.ok(body);
    }

    @DeleteMapping("{id}")
    public void deleteById(@PathVariable Integer id) {
        userService.deleteById(id);
    }

    @PutMapping("{id}")
    public ResponseEntity<User> update(@PathVariable Integer id, @Valid @RequestBody User user) {
        user.setId(id);
        user = userService.update(user);
        return ResponseEntity.ok(user);
    }

    @GetMapping("{id}/subjects")
    public ResponseEntity<Set<Subject>> getSubjectsByUserId(@PathVariable Integer id) {
        return ResponseEntity.ok(userService.findById(id).getSubjects());
    }
}
