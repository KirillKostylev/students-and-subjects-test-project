package by.pstlabs.studentsandsubjectstestproject.controller;

import by.pstlabs.studentsandsubjectstestproject.model.Subject;
import by.pstlabs.studentsandsubjectstestproject.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static by.pstlabs.studentsandsubjectstestproject.service.impl.SubjectServiceImpl.DEFAULT_PAGE_NUMBER_STR;
import static by.pstlabs.studentsandsubjectstestproject.service.impl.SubjectServiceImpl.DEFAULT_PAGE_SIZE_STR;


@RestController
@RequestMapping("subjects")
public class SubjectController {

    @Autowired
    private SubjectService subjectService;

    @GetMapping
    public ResponseEntity<Page<Subject>> findAll(
            @RequestParam(value = "pageNumber", defaultValue = DEFAULT_PAGE_NUMBER_STR) int pageNumber,
            @RequestParam(value = "pageSize", defaultValue = DEFAULT_PAGE_SIZE_STR) int pageSize,
            @RequestParam(value = "direction", defaultValue = "Asc") String direction,
            @RequestParam(value = "sortBy", defaultValue = Subject.ID_STR) String sortBy) {
        Page<Subject> page = subjectService.findPage(pageNumber, pageSize, direction, sortBy);
        return ResponseEntity.ok(page);
    }

    @GetMapping("{id}")
    public ResponseEntity<Subject> findById(@PathVariable Integer id) {
        return ResponseEntity.ok(subjectService.findById(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Subject> create(@Valid @RequestBody Subject subject) {
        return ResponseEntity.ok(subjectService.create(subject));
    }

    @DeleteMapping("{id}")
    public void deleteById(@PathVariable Integer id) {
        subjectService.deleteById(id);
    }

    @PutMapping("{id}")
    public ResponseEntity<Subject> update(@PathVariable Integer id, @Valid @RequestBody Subject subject) {
        subject.setId(id);
        subject = subjectService.update(subject);
        return ResponseEntity.ok(subject);
    }

}


