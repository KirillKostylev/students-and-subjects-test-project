package by.pstlabs.studentsandsubjectstestproject.controller;

import by.pstlabs.studentsandsubjectstestproject.service.exception.EntryNotFoundException;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@ControllerAdvice
public class ControllerExceptionHandlingAdvice {

    private static final String VALIDATION_EXCEPTION_MSG = "Validation exception";

    @ExceptionHandler(EntryNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected ResponseEntity<OutputException> handleEntryNotFoundException(EntryNotFoundException e) {
        OutputException outputException = new OutputException(HttpStatus.NOT_FOUND.name(), e.getMessage());
        log.debug("Exception = {}", outputException);
        return ResponseEntity.ok(outputException);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<OutputException> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        BindingResult bindingResult = e.getBindingResult();
        Map<String, List<String>> errorMap = parseBindingResult(bindingResult);
        OutputException outputException =
                new OutputException(HttpStatus.BAD_REQUEST.name(), VALIDATION_EXCEPTION_MSG, errorMap);
        log.debug("Exception = {}", outputException);
        return ResponseEntity.ok(outputException);
    }

    private Map<String, List<String>> parseBindingResult(BindingResult bindingResult) {
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        Map<String, List<String>> errorMap = new HashMap<>();
        for (FieldError error : fieldErrors) {
            if (!errorMap.containsKey(error.getField())) {
                errorMap.put(error.getField(), new ArrayList<>());
            }

            errorMap.get(error.getField()).add(error.getDefaultMessage());
        }
        return errorMap;
    }

    @Data
    @AllArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private static class OutputException {
        private String status;
        private String message;
        private Map<String, List<String>> errors;

        public OutputException(String status, String message) {
            this.status = status;
            this.message = message;
        }
    }
}
