package by.pstlabs.studentsandsubjectstestproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

@SpringBootApplication
public class StudentsAndSubjectsTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentsAndSubjectsTestProjectApplication.class, args);
	}
	@Bean
	public LocalValidatorFactoryBean validator() {
		return new LocalValidatorFactoryBean();
	}

}
