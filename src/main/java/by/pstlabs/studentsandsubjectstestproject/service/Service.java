package by.pstlabs.studentsandsubjectstestproject.service;

public interface Service<E, I> {
    E findById(I id);

    Iterable<E> findAll();

    E create(E entity);

    E update(E entity);

    void deleteById(I id);
}
