package by.pstlabs.studentsandsubjectstestproject.service;

import by.pstlabs.studentsandsubjectstestproject.model.Subject;
import org.springframework.data.domain.Page;

public interface SubjectService extends Service<Subject, Integer> {

    Page<Subject> findPage(int pageNumber, int pageSize, String direction, String sortBy);
}
