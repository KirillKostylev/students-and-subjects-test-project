package by.pstlabs.studentsandsubjectstestproject.service;

import by.pstlabs.studentsandsubjectstestproject.model.Role;

public interface RoleService extends Service<Role, Integer> {
    Role findByName(String name);
}
