package by.pstlabs.studentsandsubjectstestproject.service;

import by.pstlabs.studentsandsubjectstestproject.model.User;

public interface UserService extends Service<User, Integer> {
    boolean isUserExist(String email);

}
