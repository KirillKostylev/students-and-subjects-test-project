package by.pstlabs.studentsandsubjectstestproject.service.impl;

import by.pstlabs.studentsandsubjectstestproject.model.Role;
import by.pstlabs.studentsandsubjectstestproject.repository.RoleRepository;
import by.pstlabs.studentsandsubjectstestproject.service.RoleService;
import by.pstlabs.studentsandsubjectstestproject.service.exception.EntryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role findById(Integer id) {
        return roleRepository
                .findById(id)
                .orElseThrow(() -> new EntryNotFoundException(String.format("Role with id = %s not found", id)));
    }

    @Override
    public Iterable<Role> findAll() {
        return roleRepository.findAll();
    }

    @Override
    public Role create(Role entity) {
        return roleRepository.save(entity);
    }

    @Override
    public Role update(Role entity) {
        boolean existsById = roleRepository.existsById(entity.getId());
        if (!existsById) {
            throw new EntryNotFoundException(String.format("Role with id = %d not found", entity.getId()));
        }
        return roleRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        boolean existsById = roleRepository.existsById(id);
        if (!existsById) {
            throw new EntryNotFoundException(String.format("Role with id = %d not found", id));
        }
        roleRepository.deleteById(id);
    }

    @Override
    public Role findByName(String name) {
        return roleRepository
                .findByName(name)
                .orElseThrow(() -> new EntryNotFoundException(String.format("Role with name = %s not found", name)));
    }
}
