package by.pstlabs.studentsandsubjectstestproject.service.impl;

import by.pstlabs.studentsandsubjectstestproject.model.ContactInfo;
import by.pstlabs.studentsandsubjectstestproject.model.RoleEnum;
import by.pstlabs.studentsandsubjectstestproject.model.User;
import by.pstlabs.studentsandsubjectstestproject.repository.UserRepository;
import by.pstlabs.studentsandsubjectstestproject.service.UserService;
import by.pstlabs.studentsandsubjectstestproject.service.exception.EntryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public User findById(Integer id) {
        return userRepository
                .findById(id)
                .orElseThrow(() -> new EntryNotFoundException(String.format("User with id = %s not found", id)));
    }

    @Override
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User create(User entity) {
        entity.setRole(RoleEnum.STUDENT.getRole());
        entity.setSubjects(new HashSet<>());
        ContactInfo ci = entity.getContactInfo();
        ci.setUser(entity);
        entity.setContactInfo(ci);
        return userRepository.save(entity);
    }

    @Override
    public User update(User entity) {
        boolean existsById = userRepository.existsById(entity.getId());

        if (!existsById) {
            throw new EntryNotFoundException(String.format("User with id = %d not found", entity.getId()));
        }
        return userRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        boolean existsById = userRepository.existsById(id);
        if (!existsById) {
            throw new EntryNotFoundException(String.format("User with id = %d not found", id));
        }
        userRepository.deleteById(id);
    }

    @Override
    public boolean isUserExist(String email) {
        return userRepository.existsByEmail(email);
    }


}
