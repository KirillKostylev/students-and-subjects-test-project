package by.pstlabs.studentsandsubjectstestproject.service.impl;

import by.pstlabs.studentsandsubjectstestproject.model.Subject;
import by.pstlabs.studentsandsubjectstestproject.repository.SubjectRepository;
import by.pstlabs.studentsandsubjectstestproject.service.SubjectService;
import by.pstlabs.studentsandsubjectstestproject.service.exception.EntryNotFoundException;
import by.pstlabs.studentsandsubjectstestproject.utils.DirectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import static by.pstlabs.studentsandsubjectstestproject.model.Subject.*;

@Service
public class SubjectServiceImpl implements SubjectService {
    public static final String DEFAULT_PAGE_NUMBER_STR = "0";
    public static final String DEFAULT_PAGE_SIZE_STR = "3";

    @Autowired
    private SubjectRepository subjectRepository;

    @Override
    public Subject findById(Integer id) {
        return subjectRepository
                .findById(id)
                .orElseThrow(() -> new EntryNotFoundException(String.format("Subject with id = %s not found", id)));
    }

    @Override
    public Iterable<Subject> findAll() {
        return subjectRepository.findAll();
    }

    @Override
    public Subject create(Subject entity) {
        return subjectRepository.save(entity);
    }

    @Override
    public Subject update(Subject entity) {
        boolean existsById = subjectRepository.existsById(entity.getId());
        if (!existsById) {
            throw new EntryNotFoundException(String.format("Subject with id = %d not found", entity.getId()));
        }
        return subjectRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        boolean existsById = subjectRepository.existsById(id);
        if (!existsById) {
            throw new EntryNotFoundException(String.format("Subject with id = %d not found", id));
        }
        subjectRepository.deleteById(id);
    }

    @Override
    public Page<Subject> findPage(int pageNumber, int pageSize, String direction, String sortBy) {
        if (!(ID_STR.equalsIgnoreCase(sortBy) || HOURS_NUMBER_STR.equalsIgnoreCase(sortBy) || NAME_STR.equalsIgnoreCase(sortBy))) {
            sortBy = ID_STR;
        }

        int defaultPageNumber = Integer.parseInt(DEFAULT_PAGE_NUMBER_STR);
        if (pageNumber < defaultPageNumber) {
            pageNumber = defaultPageNumber;
        }

        int defaultPageSize = Integer.parseInt(DEFAULT_PAGE_SIZE_STR);
        if (pageSize < defaultPageSize) {
            pageSize = defaultPageSize;
        }

        PageRequest of = PageRequest.of(pageNumber, pageSize, DirectionUtils.getDirection(direction), sortBy);
        return subjectRepository.findAll(of);
    }
}
