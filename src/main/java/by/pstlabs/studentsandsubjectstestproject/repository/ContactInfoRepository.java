package by.pstlabs.studentsandsubjectstestproject.repository;

import by.pstlabs.studentsandsubjectstestproject.model.ContactInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactInfoRepository extends JpaRepository<ContactInfo, Integer> {
}
