package by.pstlabs.studentsandsubjectstestproject.repository;

import by.pstlabs.studentsandsubjectstestproject.model.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SubjectRepository extends PagingAndSortingRepository<Subject, Integer> {
    Page<Subject> findAll(Pageable pageable);
}
