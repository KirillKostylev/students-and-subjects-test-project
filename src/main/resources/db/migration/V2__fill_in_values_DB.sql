INSERT INTO role(id, name) VALUES (1, 'Admin');
INSERT INTO role(id, name) VALUES (2, 'Student');

INSERT INTO subject(id, name, hours_number) VALUES (1, 'Math', 300);
INSERT INTO subject(id, name, hours_number) VALUES (2, 'Physics', 250);
INSERT INTO subject(id, name, hours_number) VALUES (3, 'Chemistry', 100);
INSERT INTO subject(id, name, hours_number) VALUES (4, 'computer science', 500);

INSERT INTO contact_info(id, first_name, last_name) VALUES (1, 'Kirill', 'Kostylev');
INSERT INTO contact_info(id, first_name, last_name) VALUES (2, 'Ivan', 'Ivanovich');
INSERT INTO contact_info(id, first_name, last_name) VALUES (3, 'Roman', 'Petrov');
INSERT INTO contact_info(id, first_name, last_name) VALUES (4, 'Anastatia', 'Tolstikova');

INSERT INTO user(id, email, password, role_id, contact_info_id) VALUES (1, 'kirillkostylev2000@gmail.com', '123123', 1, 1);
INSERT INTO user(id, email, password, role_id, contact_info_id) VALUES (2, 'Ivan@gmail.com', '123123', 2, 2);
INSERT INTO user(id, email, password, role_id, contact_info_id) VALUES (3, 'RomIv@yandex.ru', '123123', 2, 3);
INSERT INTO user(id, email, password, role_id, contact_info_id) VALUES (4, 'NastTo@gmail.com', '123123', 2, 4);

INSERT INTO user_subject(user_id, subject_id) VALUES (2, 1);
INSERT INTO user_subject(user_id, subject_id) VALUES (2, 2);
INSERT INTO user_subject(user_id, subject_id) VALUES (2, 4);
INSERT INTO user_subject(user_id, subject_id) VALUES (3, 3);
INSERT INTO user_subject(user_id, subject_id) VALUES (3, 1);
INSERT INTO user_subject(user_id, subject_id) VALUES (3, 2);
INSERT INTO user_subject(user_id, subject_id) VALUES (3, 4);
INSERT INTO user_subject(user_id, subject_id) VALUES (4, 4);
INSERT INTO user_subject(user_id, subject_id) VALUES (4, 1);
INSERT INTO user_subject(user_id, subject_id) VALUES (4, 3);